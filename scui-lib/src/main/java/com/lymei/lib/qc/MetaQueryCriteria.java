package com.lymei.lib.qc;

import com.lymei.common.qc.BaseQueryCriteria;
import lombok.Data;

/**
 * @author lymei
 * @title: MetaQueryCriteria
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2613:35
 */
@Data
public class MetaQueryCriteria extends BaseQueryCriteria {

    private Long menuId;
}

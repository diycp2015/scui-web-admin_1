package com.lymei.core.modules.system.mapper;

import com.lymei.common.mapper.IBaseMapper;
import com.lymei.lib.po.system.RoleMenuRelation;
import com.lymei.lib.vo.system.RoleMenuRelationVo;
import com.lymei.lib.vo.system.RoleVo;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author lymei
 * @title: IRoleMenuRelation
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2311:59
 */
public interface IRoleMenuRelationMapper extends IBaseMapper<RoleMenuRelation> {

    /**
     * 根据角色ID查询
     * @param roles
     * @return
     */
    List<RoleMenuRelationVo> selectRelationByRoles(@Param("roles") Set<RoleVo> roles);
}

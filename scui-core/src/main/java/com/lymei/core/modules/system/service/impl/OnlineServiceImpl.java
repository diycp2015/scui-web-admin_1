package com.lymei.core.modules.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.lymei.common.utils.RedisUtils;
import com.lymei.core.modules.system.service.IOnlineService;
import com.lymei.lib.vo.system.UserVo;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author lymei
 * @title: OnlineServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2411:05
 */
@Service
public class OnlineServiceImpl implements IOnlineService {

    private static final String onlineKeyPrefix = "Online_";

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public void put(String token,UserVo user) {
        redisUtils.set(onlineKeyPrefix+token,user);
    }

    @Override
    public void remove(String token) {
        redisUtils.del(onlineKeyPrefix+token);
    }


    @Override
    public List<UserVo> online() {
        var keys = redisUtils.likeKeys(onlineKeyPrefix);
        List<UserVo> userVos = new ArrayList<>();
        for (String s : Optional.ofNullable(keys).orElse(new LinkedHashSet<>())) {
            userVos.add(this.get(s));
        }
        return userVos;
    }

    @Override
    public boolean has(String token) {
        return redisUtils.hasKey(onlineKeyPrefix+token);
    }

    @Override
    public UserVo get(String token) {
        if (this.has(token)){
            return (UserVo) redisUtils.get(onlineKeyPrefix+token);
        }
        return null;
    }

    @Override
    public UserVo get(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String prefix = "Bearer ";
        if (StringUtils.isBlank(token)){
            return null;
        }
        if (!token.contains(prefix)){
            return null;
        }
        token = token.replace(prefix,"");
        if (this.has(token)){
            return this.get(token);
        }
        return null;
    }
}

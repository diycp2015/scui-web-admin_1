package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: RoleMenuRelation
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2114:27
 */
@Data
@Entity
@Table(name = "sys_roles_menus")
@TableName(value = "sys_roles_menus")
public class RoleMenuRelation extends BasePo {

    private Long roleId;

    private Long menuId;

}

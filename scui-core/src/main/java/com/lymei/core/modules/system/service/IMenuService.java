package com.lymei.core.modules.system.service;

import com.lymei.common.service.IBaseService;
import com.lymei.lib.po.system.Menu;
import com.lymei.lib.vo.system.MenuVo;

import java.util.List;

/**
 * @author lymei
 * @title: IScuiMenuService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:28
 */
public interface IMenuService extends IBaseService<Menu> {

    List<MenuVo> selectMenuByUserId(Long userId);

    List<MenuVo> selectList();
}

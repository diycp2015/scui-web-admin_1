package com.lymei.core.modules.system.service;

/**
 * @author lymei
 * @title: IDemoService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2615:32
 */
public interface IDemoService {

    String getVer();
}

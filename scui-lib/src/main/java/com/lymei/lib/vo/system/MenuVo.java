package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.Menu;
import lombok.Data;

import java.util.List;

/**
 * @author lymei
 * @title: ScuiMenuVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:12
 */
@Data
public class MenuVo extends Menu {

    private List<MenuVo> children;

    private MetaVo meta;
}

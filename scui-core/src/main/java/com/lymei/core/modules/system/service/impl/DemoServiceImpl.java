package com.lymei.core.modules.system.service.impl;

import com.lymei.core.modules.system.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author lymei
 * @title: DemoServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2615:33
 */
@Service
public class DemoServiceImpl implements IDemoService {

    @Value("${app.version:1.0.0}")
    private String ver;

    @Override
    public String getVer() {
        return ver;
    }
}

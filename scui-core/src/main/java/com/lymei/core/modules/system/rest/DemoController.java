package com.lymei.core.modules.system.rest;

import com.lymei.common.po.ResultResponse;
import com.lymei.core.modules.system.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lymei
 * @title: DomeController
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2615:31
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private IDemoService demoService;

    @GetMapping("/ver")
    public ResultResponse getVer(){
        return ResultResponse.success(demoService.getVer());
    }
}

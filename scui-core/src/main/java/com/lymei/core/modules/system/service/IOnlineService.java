package com.lymei.core.modules.system.service;

import com.lymei.lib.vo.system.UserVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author lymei
 * @title: IOnlineService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2411:01
 */
public interface IOnlineService {


    void put(String token,UserVo user);

    void remove(String token);

    List<UserVo> online();

    boolean has(String token);

    UserVo get(String token);

    UserVo get(HttpServletRequest request);
}

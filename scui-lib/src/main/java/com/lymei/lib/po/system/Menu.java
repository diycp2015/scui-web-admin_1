package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: ScuiMenu
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:08
 */
@Data
@Entity
@Table(name = "sys_menu")
@TableName("sys_menu")
public class Menu extends BasePo {

    private String name;

    private String path;

    private Long pid;

    private String component;

    private String redirect;

}

package com.lymei.core.modules.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.lymei.common.enums.Del;
import com.lymei.common.enums.Status;
import com.lymei.common.exception.BusinessException;
import com.lymei.common.service.impl.BaseServiceImpl;
import com.lymei.common.utils.IdWork;
import com.lymei.core.modules.system.mapper.IUserMapper;
import com.lymei.core.modules.system.service.IUserService;
import com.lymei.lib.po.system.User;
import com.lymei.lib.vo.system.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author lymei
 * @title: UserServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2116:14
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<IUserMapper, User> implements IUserService {


    @Autowired
    private IdWork idWork;

    /**
     * 校验账号是否唯一
     * @param account
     * @return
     */
    private boolean verificationAccountUnique(String account){
        return baseMapper.selectAccountByAccount(account) <= 0;
    }

    /**
     * 创建新的
     * @param userVo
     * @return
     */
    @Override
    public UserVo create(UserVo userVo) {
        if (userVo == null){
            throw new BusinessException("用户注册信息不全");
        }
        if (StringUtils.isBlank(userVo.getAccount())){
            throw new BusinessException("账号不能为空");
        }
        if (StringUtils.isBlank(userVo.getUserName())){
            throw new BusinessException("用户名不能为空");
        }
        if (StringUtils.isBlank(userVo.getPassword())){
            throw new BusinessException("密码不能为空");
        }
        if (!verificationAccountUnique(userVo.getAccount())){
            throw new BusinessException("该账号已被注册");
        }
        userVo.setId(idWork.nextId());
        userVo.setPassword(userVo.getPassword());
        userVo.setStatus(Status.ENABLE);
        userVo.setDel(Del.E);
        baseMapper.insert(userVo);
        userVo.setPassword("******");
        return userVo;
    }
}

package com.lymei.common.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author lymei
 * @title: BaseService
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2016:11
 */
public interface IBaseService<T> extends IService<T> {
}

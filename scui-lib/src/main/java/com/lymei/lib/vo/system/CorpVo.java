package com.lymei.lib.vo.system;

import com.lymei.lib.po.system.Corp;
import lombok.Data;

/**
 * @author lymei
 * @title: CorpVo
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2114:34
 */
@Data
public class CorpVo extends Corp {
}

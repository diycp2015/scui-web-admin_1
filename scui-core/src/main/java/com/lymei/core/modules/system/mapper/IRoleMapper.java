package com.lymei.core.modules.system.mapper;

import com.lymei.common.mapper.IBaseMapper;
import com.lymei.lib.po.system.Role;
import com.lymei.lib.vo.system.RoleVo;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author lymei
 * @title: IRoleMapper
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2117:22
 */
public interface IRoleMapper extends IBaseMapper<Role> {

    /**
     * 根据用户ID查询拥有的角色
     * @param userId
     * @return
     */
    List<RoleVo> selectRolesByUserId(Long userId);
}

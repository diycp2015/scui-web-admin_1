package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import com.sun.org.glassfish.gmbal.Description;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: Role
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:11
 */
@Data
@Entity
@Table(name = "sys_role")
@TableName("sys_role")
public class Role extends BasePo {

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色级别
     */
    private Integer level;

    /**
     * 描述
     */
    private String description;

    /**
     * 数据权限
     */
    private String dataScope;

}

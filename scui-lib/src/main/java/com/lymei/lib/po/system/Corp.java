package com.lymei.lib.po.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lymei.common.po.BasePo;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author lymei
 * @title: Corp
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2114:32
 */
@Data
@Entity
@Table(name = "sys_corp")
@TableName(value = "sys_corp")
public class Corp extends BasePo {

    private String name;

    private String shortName;

    private String linker;

    private String linkerPhone;

    private String address;
}

package com.lymei.core.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.lymei.common.po.ResultResponse;
import org.apache.ibatis.reflection.wrapper.ObjectWrapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lymei
 * @title: UnauthorizedHandler
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2011:36
 */
public class UnauthorizedHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        response.getWriter().print(new ObjectMapper().writeValueAsString(ResultResponse.fail("未鉴权")));
    }
}

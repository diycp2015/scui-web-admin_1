package com.lymei.core.modules.system.mapper;

import com.lymei.common.mapper.IBaseMapper;
import com.lymei.lib.po.system.Menu;
import com.lymei.lib.qc.MenuQueryCriteria;
import com.lymei.lib.vo.system.MenuVo;

import java.util.List;

/**
 * @author lymei
 * @title: IScuiMenuMapper
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:27
 */
public interface IMenuMapper extends IBaseMapper<Menu> {


    List<MenuVo> selectMenusByUserId(MenuQueryCriteria queryCriteria);


    List<MenuVo> selectChildren(MenuQueryCriteria queryCriteria);


    List<MenuVo> selectFillMenus();

    List<MenuVo> selectFillChildren(MenuQueryCriteria queryCriteria);

}

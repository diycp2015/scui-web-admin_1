package com.lymei.common.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lymei.common.mapper.IBaseMapper;
import com.lymei.common.service.IBaseService;


/**
 * @author lymei
 * @title: BaseServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2016:15
 */
public class BaseServiceImpl<M extends IBaseMapper<T>,T> extends ServiceImpl<M,T> implements IBaseService<T> {

}

package com.lymei.core.modules.system.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lymei.common.exception.BusinessException;
import com.lymei.common.service.impl.BaseServiceImpl;
import com.lymei.common.utils.IdWork;
import com.lymei.core.modules.system.mapper.IMetaMapper;
import com.lymei.core.modules.system.mapper.IRoleMenuRelationMapper;
import com.lymei.core.modules.system.mapper.IMenuMapper;
import com.lymei.core.modules.system.service.IMenuService;
import com.lymei.lib.po.system.Menu;
import com.lymei.lib.qc.MenuQueryCriteria;
import com.lymei.lib.vo.system.RoleMenuRelationVo;
import com.lymei.lib.vo.system.MenuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lymei
 * @title: ScuiMenuServiceImpl
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/2611:29
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<IMenuMapper, Menu> implements IMenuService {
    @Override
    public List<MenuVo> selectMenuByUserId(Long userId) {
        if (userId == null){
            throw new BusinessException("缺少用户信息");
        }
        MenuQueryCriteria queryCriteria = new MenuQueryCriteria();
        queryCriteria.setUserId(userId);
        return baseMapper.selectMenusByUserId(queryCriteria);
    }

    @Override
    public List<MenuVo> selectList() {
        return baseMapper.selectFillMenus();
    }


//    @Autowired
//    private IdWork idWork;




//    @Autowired
//    private IMenuMapper scuiMenuMapper;
//
//    @Autowired
//    private IMetaMapper metaMapper;
//
//    @Autowired
//    private IRoleMenuRelationMapper roleMenuRelationMapper;




//    @Override
//    public void g() {
//        String json = "[\n" +
//                "            {\n" +
//                "                \"name\":\"home\",\n" +
//                "                \"path\":\"/home\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"首页\",\n" +
//                "                    \"icon\":\"el-icon-eleme-filled\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"name\":\"dashboard\",\n" +
//                "                        \"path\":\"/dashboard\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"控制台\",\n" +
//                "                            \"icon\":\"el-icon-menu\",\n" +
//                "                            \"affix\":true\n" +
//                "                        },\n" +
//                "                        \"component\":\"home\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"name\":\"userCenter\",\n" +
//                "                        \"path\":\"/usercenter\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"个人信息\",\n" +
//                "                            \"icon\":\"el-icon-user\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"userCenter\"\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"name\":\"vab\",\n" +
//                "                \"path\":\"/vab\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"组件\",\n" +
//                "                    \"icon\":\"el-icon-takeaway-box\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/mini\",\n" +
//                "                        \"name\":\"minivab\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"原子组件\",\n" +
//                "                            \"icon\":\"el-icon-magic-stick\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"vab/mini\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/iconfont\",\n" +
//                "                        \"name\":\"iconfont\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"扩展图标\",\n" +
//                "                            \"icon\":\"el-icon-orange\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"vab/iconfont\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/data\",\n" +
//                "                        \"name\":\"vabdata\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"Data 数据展示\",\n" +
//                "                            \"icon\":\"el-icon-histogram\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/chart\",\n" +
//                "                                \"name\":\"chart\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"图表 Echarts\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/chart\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/statistic\",\n" +
//                "                                \"name\":\"statistic\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"统计数值\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/statistic\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/video\",\n" +
//                "                                \"name\":\"scvideo\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"视频播放器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/video\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/form\",\n" +
//                "                        \"name\":\"vabform\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"Form 数据录入\",\n" +
//                "                            \"icon\":\"el-icon-edit\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/tableselect\",\n" +
//                "                                \"name\":\"tableselect\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"表格选择器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/tableselect\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/formtable\",\n" +
//                "                                \"name\":\"formtable\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"表单表格\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/formtable\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/filterbar\",\n" +
//                "                                \"name\":\"filterBar\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"过滤器v2\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/filterBar\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/upload\",\n" +
//                "                                \"name\":\"upload\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"上传\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/upload\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/select\",\n" +
//                "                                \"name\":\"scselect\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"异步选择器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/select\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/iconselect\",\n" +
//                "                                \"name\":\"iconSelect\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"图标选择器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/iconselect\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/editor\",\n" +
//                "                                \"name\":\"editor\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"富文本编辑器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/editor\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/feedback\",\n" +
//                "                        \"name\":\"vabfeedback\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"Feedback 反馈\",\n" +
//                "                            \"icon\":\"el-icon-mouse\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/drag\",\n" +
//                "                                \"name\":\"drag\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"拖拽排序\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/drag\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/contextmenu\",\n" +
//                "                                \"name\":\"contextmenu\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"右键菜单\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/contextmenu\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/cropper\",\n" +
//                "                                \"name\":\"cropper\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"图像剪裁\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/cropper\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/fileselect\",\n" +
//                "                                \"name\":\"fileselect\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"资源库选择器\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/fileselect\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/dialog\",\n" +
//                "                                \"name\":\"dialogExtend\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"弹窗扩展\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/dialog\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/others\",\n" +
//                "                        \"name\":\"vabothers\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"Others 其他\",\n" +
//                "                            \"icon\":\"el-icon-more-filled\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/vab/print\",\n" +
//                "                                \"name\":\"print\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"打印\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"vab/print\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/workflow\",\n" +
//                "                        \"name\":\"workflow\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"工作流设计器\",\n" +
//                "                            \"icon\":\"el-icon-share\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"vab/workflow\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/vab/formrender\",\n" +
//                "                        \"name\":\"formRender\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"动态表单(Beta)\",\n" +
//                "                            \"icon\":\"el-icon-message-box\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"vab/form\"\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"name\":\"template\",\n" +
//                "                \"path\":\"/template\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"模板\",\n" +
//                "                    \"icon\":\"el-icon-files\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/blank\",\n" +
//                "                        \"name\":\"blank\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"空白模板\",\n" +
//                "                            \"icon\":\"el-icon-folder\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/blank\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/chartlist\",\n" +
//                "                        \"name\":\"chartlist\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"统计列表\",\n" +
//                "                            \"icon\":\"el-icon-data-analysis\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/chartlist\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/calendar\",\n" +
//                "                        \"name\":\"calendar\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"日历计划\",\n" +
//                "                            \"icon\":\"el-icon-calendar\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/calendar\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/list\",\n" +
//                "                        \"name\":\"list\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"详细列表\",\n" +
//                "                            \"icon\":\"el-icon-fold\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/list\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/list/save/:id?\",\n" +
//                "                        \"name\":\"list-save\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"新标签\",\n" +
//                "                            \"hidden\":true,\n" +
//                "                            \"active\":\"/template/list\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/list/save\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/svgmap\",\n" +
//                "                        \"name\":\"svgmap\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"地理信息\",\n" +
//                "                            \"icon\":\"el-icon-map-location\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/svgmap\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/tabinfo\",\n" +
//                "                        \"name\":\"tabinfo\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"分栏明细\",\n" +
//                "                            \"icon\":\"el-icon-document\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/tabinfo\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/server\",\n" +
//                "                        \"name\":\"server\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"服务器监控\",\n" +
//                "                            \"icon\":\"el-icon-cpu\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/server\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/template/stepform\",\n" +
//                "                        \"name\":\"stepform\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"分步表单\",\n" +
//                "                            \"icon\":\"el-icon-switch\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"template/stepform\"\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"name\":\"other\",\n" +
//                "                \"path\":\"/other\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"其他\",\n" +
//                "                    \"icon\":\"el-icon-more-filled\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"path\":\"/other/directive\",\n" +
//                "                        \"name\":\"directive\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"指令\",\n" +
//                "                            \"icon\":\"el-icon-price-tag\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"other/directive\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/other/viewTags\",\n" +
//                "                        \"name\":\"viewTags\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"标签操作\",\n" +
//                "                            \"icon\":\"el-icon-files\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"other/viewTags\",\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/other/fullpage\",\n" +
//                "                                \"name\":\"fullpage\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"整页路由\",\n" +
//                "                                    \"icon\":\"el-icon-monitor\",\n" +
//                "                                    \"fullpage\":true,\n" +
//                "                                    \"hidden\":true,\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"other/fullpage\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/link\",\n" +
//                "                        \"name\":\"link\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"外部链接\",\n" +
//                "                            \"icon\":\"el-icon-link\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"https://baidu.com\",\n" +
//                "                                \"name\":\"百度\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"百度\",\n" +
//                "                                    \"type\":\"link\"\n" +
//                "                                }\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"https://www.google.cn\",\n" +
//                "                                \"name\":\"谷歌\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"谷歌\",\n" +
//                "                                    \"type\":\"link\"\n" +
//                "                                }\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/iframe\",\n" +
//                "                        \"name\":\"Iframe\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"Iframe\",\n" +
//                "                            \"icon\":\"el-icon-position\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"https://v3.cn.vuejs.org\",\n" +
//                "                                \"name\":\"vue3\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"VUE 3\",\n" +
//                "                                    \"type\":\"iframe\"\n" +
//                "                                }\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"https://element-plus.gitee.io\",\n" +
//                "                                \"name\":\"elementplus\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"Element Plus\",\n" +
//                "                                    \"type\":\"iframe\"\n" +
//                "                                }\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"https://lolicode.gitee.io/scui-doc\",\n" +
//                "                                \"name\":\"scuidoc\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"SCUI文档\",\n" +
//                "                                    \"type\":\"iframe\"\n" +
//                "                                }\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"name\":\"test\",\n" +
//                "                \"path\":\"/test\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"实验室\",\n" +
//                "                    \"icon\":\"el-icon-mouse\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"path\":\"/test/autocode\",\n" +
//                "                        \"name\":\"autocode\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"代码生成器\",\n" +
//                "                            \"icon\":\"sc-icon-code\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"redirect\":\"/test/autocode/index\",\n" +
//                "                        \"children\":[\n" +
//                "                            {\n" +
//                "                                \"path\":\"/test/autocode/index\",\n" +
//                "                                \"name\":\"autocode-index\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"代码生成器\",\n" +
//                "                                    \"hidden\":true,\n" +
//                "                                    \"hiddenBreadcrumb\":true,\n" +
//                "                                    \"active\":\"/test/autocode\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"test/autocode\"\n" +
//                "                            },\n" +
//                "                            {\n" +
//                "                                \"path\":\"/test/autocode/list\",\n" +
//                "                                \"name\":\"autocode-list\",\n" +
//                "                                \"meta\":{\n" +
//                "                                    \"title\":\"列表生成器\",\n" +
//                "                                    \"hidden\":true,\n" +
//                "                                    \"active\":\"/test/autocode\",\n" +
//                "                                    \"type\":\"menu\"\n" +
//                "                                },\n" +
//                "                                \"component\":\"test/autocode/list\"\n" +
//                "                            }\n" +
//                "                        ]\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/test/codebug\",\n" +
//                "                        \"name\":\"codebug\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"异常处理\",\n" +
//                "                            \"icon\":\"sc-icon-bug-line\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"test/codebug\"\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"name\":\"setting\",\n" +
//                "                \"path\":\"/setting\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"配置\",\n" +
//                "                    \"icon\":\"el-icon-setting\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"children\":[\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/system\",\n" +
//                "                        \"name\":\"system\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"系统设置\",\n" +
//                "                            \"icon\":\"el-icon-tools\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/system\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/user\",\n" +
//                "                        \"name\":\"user\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"用户管理\",\n" +
//                "                            \"icon\":\"el-icon-user-filled\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/user\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/role\",\n" +
//                "                        \"name\":\"role\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"角色管理\",\n" +
//                "                            \"icon\":\"el-icon-notebook\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/role\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/dic\",\n" +
//                "                        \"name\":\"dic\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"字典管理\",\n" +
//                "                            \"icon\":\"el-icon-document\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/dic\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/table\",\n" +
//                "                        \"name\":\"tableSetting\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"表格列管理\",\n" +
//                "                            \"icon\":\"el-icon-scale-to-original\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/table\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/menu\",\n" +
//                "                        \"name\":\"settingMenu\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"菜单管理\",\n" +
//                "                            \"icon\":\"el-icon-fold\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/menu\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/task\",\n" +
//                "                        \"name\":\"task\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"计划任务\",\n" +
//                "                            \"icon\":\"el-icon-alarm-clock\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/task\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/client\",\n" +
//                "                        \"name\":\"client\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"应用管理\",\n" +
//                "                            \"icon\":\"el-icon-help-filled\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/client\"\n" +
//                "                    },\n" +
//                "                    {\n" +
//                "                        \"path\":\"/setting/log\",\n" +
//                "                        \"name\":\"log\",\n" +
//                "                        \"meta\":{\n" +
//                "                            \"title\":\"系统日志\",\n" +
//                "                            \"icon\":\"el-icon-warning\",\n" +
//                "                            \"type\":\"menu\"\n" +
//                "                        },\n" +
//                "                        \"component\":\"setting/log\"\n" +
//                "                    }\n" +
//                "                ]\n" +
//                "            },\n" +
//                "            {\n" +
//                "                \"path\":\"/other/about\",\n" +
//                "                \"name\":\"about\",\n" +
//                "                \"meta\":{\n" +
//                "                    \"title\":\"关于\",\n" +
//                "                    \"icon\":\"el-icon-info-filled\",\n" +
//                "                    \"type\":\"menu\"\n" +
//                "                },\n" +
//                "                \"component\":\"other/about\"\n" +
//                "            }\n" +
//                "        ]";
//
//        try {
//            List<MenuVo> tree = new ObjectMapper().readValue(json, new TypeReference<List<MenuVo>>(){});
//
//
//            List<MenuVo> menus = this.set(tree,null);
//
//            List<Long> ids = new ArrayList<>();
//            this.searchMenuIds(menus,ids);
//
//            for (Long id : ids) {
//                RoleMenuRelationVo roleMenuRelationVo = new RoleMenuRelationVo();
//                roleMenuRelationVo.setId(idWork.nextId());
//                roleMenuRelationVo.setRoleId(1L);
//                roleMenuRelationVo.setMenuId(id);
//                roleMenuRelationMapper.insert(roleMenuRelationVo);
//            }
//
//
//
//            this.insert(menus);
//
//
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//
//    private List<MenuVo> set(List<MenuVo> menu, Long pid){
//        List<MenuVo> list = new ArrayList<>();
//        for (MenuVo menuVo : menu) {
//            menuVo.setId(idWork.nextId());
//            menuVo.setPid(pid);
//            menuVo.getMeta().setMenuId(menuVo.getId());
//            menuVo.getMeta().setId(idWork.nextId());
//            if (menuVo.getChildren() != null){
//                menuVo.setChildren(this.set(menuVo.getChildren(), menuVo.getId()));
//            }
//            list.add(menuVo);
//        }
//        return list;
//    }
//
//    private void insert(List<MenuVo> menus){
//        for (MenuVo menu : menus) {
//            scuiMenuMapper.insert(menu);
//            metaMapper.insert(menu.getMeta());
//            if (menu.getChildren() != null){
//                this.insert(menu.getChildren());
//            }
//        }
//    }
//
//    private void searchMenuIds(List<MenuVo> menus, List<Long> ids){
//
//        for (MenuVo menu : menus) {
//            ids.add(menu.getId());
//            if (menu.getChildren()!=null){
//                this.searchMenuIds(menu.getChildren(),ids);
//            }
//        }
//    }
}

package com.lymei.core.modules.system.mapper;

import com.lymei.common.mapper.IBaseMapper;
import com.lymei.lib.po.system.User;
import com.lymei.lib.vo.system.UserVo;

/**
 * @author lymei
 * @title: UserMapper
 * @projectName scui-admin
 * @description: TODO
 * @date 2022/1/219:42
 */
public interface IUserMapper extends IBaseMapper<User> {

    /**
     * 查询账号条目数
     * 用于校验账号重复性
     * @param account
     * @return
     */
    int selectAccountByAccount(String account);

    /**
     * 根据账号查询用户
     * @param account
     * @return
     */
    UserVo selectUserByAccount(String account);
}
